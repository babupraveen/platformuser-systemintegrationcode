let AWS = require("aws-sdk");
AWS.config.region = 'us-east-1'; // Region
AWS.config.credentials = new AWS.CognitoIdentityCredentials({
    IdentityPoolId: 'us-east-1:e73cb695-9300-46b3-96d1-f33ba6422509',
});
let docClient = new AWS.DynamoDB.DocumentClient();

let sfdcdataObject = require('./genericjsonforsystemintegraion.json');
console.log(JSON.stringify(sfdcdataObject));
let elicitSlot = {
    "dialogState": "ElicitSlot",
    "intentName": "HR_ApplyLeave",
    "message": "For how many days you would like to apply leave?",
    "messageFormat": "PlainText",
    "responseCard": null,
    "sessionAttributes": {
        "intentName": "HR_ApplyLeave",
        "slots": "{\"leavetype\":null,\"enddate\":null,\"noofdays\":null,\"startdate\":null}"
    },
    "slotToElicit": "enddate",
    "slots": {
        "enddate": null,
        "leavetype": 'casual',
        "noofdays": null,
        "startdate": null
    }
};
let elicitIntent = {
    "dialogState": "ElicitIntent",
    "intentName": null,
    "message": "Your application has been submitted and under approval process of your manager. Your leave application number is: 2201. You will getting notified once approved by your manager.",
    "messageFormat": "PlainText",
    "responseCard": null,
    "sessionAttributes": {
        "intentName": "HR_ApplyLeave",
        "slots": "{\"leavetype\":\"today\",\"enddate\":\"2018-11-29\",\"noofdays\":\"2\",\"startdate\":\"2018-11-29\"}"
    },
    "slotToElicit": null,
    "slots": null
};
let elicitIntentFalse = {
    "dialogState": "ElicitIntent",
    "intentName": null,
    "message": "Linked with system integration",
    "messageFormat": "PlainText",
    "responseCard": null,
    "sessionAttributes": {
        "intentName": "Sales_Leads",
        "slots": "{\"leadindustry\":\"Banking\",\"leadcount\":null}"
    },
    "slotToElicit": null,
    "slots": null
};

let promptMap = {
    "system_config": "null",
    "skillid": "2",
    "response_config": {
        "responsetype": "text",
        "responsekeyword": null
    },
    "skill_name": "HR_Leave",
    "intent_name": "HR_ApplyLeave",
    "intentid": "5",
    "slots_answer_config": {
        "issloltsrequiredtofulfill": true,
        "slotsmap": [{
                "slotname": "noofdays",
                "question": "Ask for no of days?",
                "answer": "For how many days you would like to apply leave?"
            },
            {
                "slotname": "leavetype",
                "question": "Ask for leave type?",
                "answer": "What would be type of leave?"
            },
            {
                "slotname": "startdate",
                "question": "Ask for start date?",
                "answer": "From which date you would like to start your leave?"
            },
            {
                "slotname": "enddate",
                "question": "Ask for end date?",
                "answer": "To which date you would like to take your leave?"
            }
        ],
        "finalanswer": "Your application has been recorded."
    },
    "intent_description": "Wana take a vacation? Need to apply leave? I am here to help you. Just ask: Apply leave."
};
let nonpropmt = {
    "system_config": {
        "sytemname": "SFDC"
    },
    "skillid": "3",
    "response_config": {
        "responsetype": "table",
        "responsekeyword": "@_SFDC.IndustryLeads"
    },
    "skill_name": "Sales_Leads",
    "intent_name": "Sales_Leads",
    "intentid": "8",
    "slots_answer_config": {
        "systemconfig": "sfdc",
        "issloltsrequiredtofulfill": false,
        "slotsmap": [{
                "slotname": "leadindustry",
                "question": "NA",
                "answer": "Here are your leads @_sfdc.IndustryLeads",
                "systemparams": "leadindustry",
                "api": "https://certificateformaya-dev-ed.my.salesforce.com/services/data/v43.0/query/?q=SELECT+name,phone+from+Lead+where+Industry+=+'Banking'",
                "method": "GET",
                "outputparam": "@_SFDC.IndustryLeads"
            },
            {
                "slotname": "leadcount",
                "question": "NA",
                "answer": "Here are your leads"
            }
        ]
    },
    "intent_description": "I will provide you the information about SFDC leads."
};


// check dialogState from Lex resposne
const dialogState = elicitIntentFalse.dialogState;
const intentName = elicitIntentFalse.sessionAttributes.intentName;
const slots = elicitIntentFalse.sessionAttributes.slots;

// to get answer based on dialogState and slot values
getFinalAnswer(slots, dialogState, intentName,function(callback){
  console.log('final answer is:=>'+callback);
  // check system type
  let message = callback;
  if(isValid(message) && message.indexOf('@_') > -1){
    let regx = /@_[\w\.]+/g;
    let extractedValues = message.match(regx);
    for (let i = 0; i <= extractedValues.length - 1; i++) { // if multiple values
        let splitresult = extractedValues[i].split('.');
          // call dynamic function
        // // TODO: check system type and hit to specific system and get the data

    }
  }

});
// let finalresult = sendFinalResponseToUser(finalAnswer, intentName);

//function to get data form dynamo db
function getIntentDataFromDb(intentname, callback) {
    console.log('intnet name is :=>' + intentname);
    try {
        let table = "Platform_Intents";
        let name = intentname;
        let params = {
            TableName: table,
            KeyConditionExpression: "intent_name = :int",
            ExpressionAttributeValues: {
                ":int": name
            }
        };
        docClient.query(params, function(err, data) {
            if (err) {
                console.error("Unable to query. Error:", JSON.stringify(err, null, 2));
            } else {
                console.log("Query succeeded.");
                callback(data);
            }
        });

    } catch (e) {

    }
}

// function to get answer
function getFinalAnswer(slots, dialogState, intentName,callback) {
    if (isValid(dialogState) && dialogState == 'ElicitSlot') {
        try {
            console.log('in elicitslot :=>');
            let slotValue = elicitSlot.slotToElicit; // from lex resposne
            getIntentDataFromDb(intentName, function(callback1) {
                if (callback1 !== null && callback1 !== '' && callback1 !== undefined) {
                    let slotsmap = JSON.parse(callback1.Items[0].slots_answer_config).slotsmap;
                    return callback(getPromptBasedOnSlotValue(slotValue,slotsmap));
                }
            });
        } catch (e) {
            console.log('Exception in getFinalAnswer :=>' + e);
        }
    } else if (isValid(dialogState) && dialogState == 'ElicitIntent') {
        try {
          console.log('in elicitintent :=>');
            // inputs from the lex resposne
            getIntentDataFromDb(intentName, function(callback1) {
              console.log('data is :)'+JSON.stringify(callback1));
                if (callback1 !== null && callback1 !== '' && callback1 !== undefined) {
                    let boolean = callback1.Items[0].issloltsrequiredtofulfill;
                    let slotsmap = JSON.parse(callback1.Items[0].slots_answer_config).slotsmap;
                    if (boolean == true) {
                        return  callback(JSON.parse(callback1.Items[0].slots_answer_config).finalanswer);
                    } else {
                         return callback(checkIsRequiredSlotsFulfilledOrNot(slots, slotsmap));
                    }
                }
            });
        } catch (e) {
            console.log('Exception in elicit intent case :=>' + e);
        }
    }
}
// function to get answer based on slot value
function getPromptBasedOnSlotValue(slotvalue, slotsmap) {
    try {
        if (isValid(slotvalue) && isValid(slotsmap)) {
            for (let i = 0; i <= slotsmap.length - 1; i++) {
                if (isValid(slotsmap[i].slotname) && slotsmap[i].slotname == slotvalue) {
                    return slotsmap[i].answer;
                }
            }
        }
    } catch (e) {
        console.log('Exception in getPromptBasedOnSlotValue :=>' + e);
    }
}
//function to get data from dynamo db
function checkIsRequiredSlotsFulfilledOrNot(slots, slotsmap) {
    try {
        let lexslots = JSON.parse(slots);
        let keys = Object.keys(lexslots);
        for (let i = 0; i <= keys.length - 1; i++) {
            if (isValid(lexslots[keys[i]])) {
                for (let j = 0; j <= slotsmap.length - 1; j++) {
                    if (isValid(slotsmap[j].slotname) && slotsmap[j].slotname == keys[i]  ) {
                        return slotsmap[j].answer;
                    }
                }
            }
        }
    } catch (e) {
        console.log('Exception in checkIsRequiredSlotsFulfilledOrNot :=>' + e);
    }
}


// dynamic function to get system data
var ans="the lead count is @sfdc_leads and leaves are @sfdc_leaves"

var extractedString="sfdc";
var resourceKeywords=["@sfdc_leads","@sfdc_leaves"];

//fetch from resource table where sysyemname=sfdc this returns data object

_.each(data.resources,function(aResource){
  if(aResource.resource_keyword in resourceKeywords){
    var url=aResource.endpoint,
    var params={};
    var methord=aResource.methord,
    _.each(aResource.parameters,function(aParameter){
      if (aParameter.type==="query"){
        var value=aParameter.value;
        url=url+""?"+aParameter.name+"="+value;
      }
      if (aParameter.type==="body"){
        var obj={
          [aParameter.name]:aParameter.value;
        }
        var params=Object.assign(params,obj);
      }
    });
    var options = {
        url: url,
        method: methord,
        body: params
    };
    request(options, function(err, res, body) {
         if (err) {

         } else{
           //send hre answar
         }

  }
});

this.getLeads=function(){
  //logic to get the values
  return value;
}

// resource jsonp
{
  "system_name":"sfdc",
  "resources":[{
    "resource_keyword":"@sfdc_leads",
    "endpoint":"https://sfdc.leads/api/",
    "methord":"POST",
    "parameters":[{
      "type":"query",
      "name":"userId",
      "value":"getUserId()"
      },
      {
        "type":"body",
        "name":"leads",
        "value":"getLeads()"
        }]
    },
    {
      "resource_keyword":"@sfdc_leaves",
      "endpoint":"https://sfdc/leaves/api/",
      "methord":"GET",
      "parameters":[{
        "type":"query",
        "name":"userId",
        "value":"getUserId()"
        }]
      }]
}

// Utility function for string
function isValid(input) {
    if (input != null && input != '' && input != undefined) {
        return true;
    }
    return false;
}

// utility function for Object
function isEmpty(obj) {
    for (let key in obj) {
        if (obj.hasOwnProperty(key))
            return false;
    }
    return true;
}
