var express = require('express'),
    cors = require('cors'),
    app = express();
app.use(cors());
var bodyParser = require('body-parser');
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}));

app.listen(8082, function() {
    console.log('CORS-enabled web server listening on port 8082');
});

app.get('/get', function(req, resp, next) {
    try{
      var jsonParams;
      var jsonSBUrl;
      var agentBaseURL;
    console.log("get request is:::"+"\n"+":body is ::"+JSON.stringify(req.query));
    if (isValid(req.query.header)) {
       // var jsonParams = JSON.parse(JSON.stringify((req.query.header)));
           jsonParams =  req.query.header;
	console.log("IN get request  jsonparams is::"+jsonParams);
    }

    if (isValid(req.query.SBUrl)) {
         jsonSBUrl  = req.query.SBUrl;
	console.log(" IN get Request JSONSBURL is:::"+jsonSBUrl);
    }
    if (isValid(req.query.agentBaseURL)) {
         agentBaseURL  = req.query.agentBaseURL;
  console.log(" IN get Request JSONSBURL is:::"+agentBaseURL);
    }
    var sessionUrl = agentBaseURL + jsonSBUrl;
    var request = require('request');
        var options = {
            uri: sessionUrl,
            method: 'GET',
            headers: jsonParams
        };
console.log('options in get request is:)'+JSON.stringify(options));
    try {
        request(options, function(err, res, body) {
                if (err) {
                     try{
                       console.log('In  first try block');
                     }catch(err){
                        console.log("error in  get request:111:::"+err);
                    }
                    console.log("got error");
                } else(res.statusCode.toString() == 200)
                {
                    try{
                    console.log("response code is: in get method)" + res.statusCode+"\n"+"total resp is  get:::"+JSON.stringify(body));
                    resp.json({
                        resp: res.statusCode,
                        //data: body == 'OK' ? JSON.stringify(body) : (isValid(body) === true) ? JSON.stringify(JSON.parse(body)) : ""
						data: (isValid(body) === true)? (body) : ""
                      });
                    }catch(e){
                        console.log("error in  get request:::222:"+e)
                    }
                }
         });
    } catch (e) {
        console.log("Exception in setLiveAgentSession :333::) ==>" + e);
    }
    }catch(e){
        console.log("jp:exception from get method  is::: 4444"+e);
    }
});

app.post('/post', function(req, resp) {
    try{
        var flag;
        var jsonParams;
        var inputtype;
        var jsonSBURl;
        var agentBaseURL;
        var jsonBody;
        console.log("post request is:::"+JSON.stringify(req.body));
		 if(isValid(req.body.urlParams)){
			 flag = req.body.urlParams.flag;
			 console.log("flag value in post method is :)"+flag);
		 }

		if (isValid(req.body.header)) {
       jsonParams = (req.body.header);
         console.log("json paaras are:"+JSON.stringify(jsonParams));
      }
    if (isValid(req.body.inputtype)) {
        inputtype =req.body.inputtype;
	console.log("input type is::::"+inputtype);
    }
    if (isValid(req.body.SBUrl)) {
         jsonSBURl = req.body.SBUrl;
    console.log("in pot request sburl is::::"+jsonSBURl);
    }
    if (isValid(req.body.agentBaseURL)) {
           agentBaseURL  = req.body.agentBaseURL;
  console.log(" IN post Request agentBaseURL is:::"+agentBaseURL);
    }
    if(isValid(req.body.body)){
      if(flag == 1){
      jsonBody = req.body.body;
      jsonBody.organizationId = isValid(req.body.organizationId)?req.body.organizationId:'';
      jsonBody.deploymentId = isValid(req.body.deploymentId)?req.body.deploymentId:'';
      jsonBody.buttonId = isValid(req.body.buttonId)?req.body.buttonId:'';
      console.log('data in post request is'+jsonBody);
    }
  }
  for (var k in jsonBody) {
       if (jsonBody[k] == '[]')
           jsonBody[k] = [];
   }
    var sessionUrl = agentBaseURL + jsonSBURl;
	console.log("in post request is session url is ::"+sessionUrl);
    var request = require('request');


	if (flag == 1) {
        var options = {
            url: sessionUrl,
            method: 'POST',
            headers: jsonParams,
            json: true,
            body: jsonBody
        };
    } else {
    var options = {
        url: sessionUrl,
        method: 'POST',
        headers: jsonParams,
        body: JSON.stringify({
            text: inputtype
        })
    };
	}

    try {
        console.log("OPTIONS IS request is :)"+JSON.stringify(options));
        request(options, function(err, res, body) {

             if (err) {
                  try{
                     }catch(err){
                        console.log("error in  get request::111::"+err);
                    }
                    console.log("got error");
                } else(res.statusCode.toString() == 200)

                {
                    try{
                    console.log("response code is: in post method)" + res.statusCode+"\n"+"tatal resp is post:::"+JSON.stringify(body));
					if(flag == '1'){
					resp.json({
                        resp: res.statusCode,
                        data: body == 'OK' ? JSON.stringify(body) : (isValid(body) === true) ? JSON.stringify(JSON.parse(body)) : ""

                    });
					}else{
						//do nothing
					}
                     }catch(err){
                        console.log("error in  get request:::222:"+err);
                    }
                }
        });
    } catch (e) {
        console.log("Exception in setLiveAgentSession :333::) ==>" + e);
    }
    }catch(e){
        console.log("exception from post request is:::444::"+e);
    }
});

function isValid(checkValue) {
    if (checkValue !== null && checkValue !== "" && checkValue !== undefined) {
        return true;
    }
    return false;
}
